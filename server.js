console.log('starting server');

var alchemyHost = "access.alchemyapi.com";
var alchemyPath = "/calls/url/";
var alchemyFullPath = ""; 
var ContentGrab = "URLGetConstraintQuery";          //API params listed here: http://www.alchemyapi.com/api/scrape/urls.html E.G. "apikey=" + APIKey + "&url=" + URLofContent + "&cquery=" + cquery or http://access.alchemyapi.com/calls/url/URLGetConstraintQuery?apikey=57cc1e180d391c0b8cce51b7cdc175bd516ea4b8&url=http://www.googe.com&cquery=google
var EntityExtraction = "URLGetRankedNamedEntities";  //API params listed here: http://www.alchemyapi.com/api/entity/urls.html E.G. don't need visual constraint or cquery, so http://access.alchemyapi.com/calls/url/URLGetRankedNamedEntities?apikey=57cc1e180d391c0b8cce51b7cdc175bd516ea4b8&url=http://www.googe.com
var KeywordExtraction = "URLGetRankedKeywords";
var IndeedHost = "http://api.indeed.com"
var PathURLIndeedJobsWashingtonDC = "/ads/apisearch?publisher=6848272948898688&l=washington%2C+dc&sort=&radius=50&st=&jt=&start=&limit=10&fromage=&filter=&latlong=1&co=us&chnl=&userip=1.2.3.4&useragent=Mozilla/%2F4.0%28Firefox%29&v=2"; // indeed XML search returning 100 recents jobs 50 miles radiius around Washington DC
//then URL of the first post is next, after that is the follow through link "see original post here". 
var URLofContent = "http://www.indeed.com/viewjob?jk=e083482a50048ca7&qd=NZfjs2w-1ggUa3Pm1xo2xC-mhTcFVl7AtpF4iZKT59A9Aywa5iCzOSgVCD7aTsK0Fd_fxgE5YnFIiokdDveKTL6VNj3qSlnC-d_b7xFSskY1Vu4aS9dH_tvzuwb-B6Jr&atk=16utat91r0k0g4fj&utm_source=publisher&utm_medium=organic_listings&utm_campaign=affiliate";
var cquery = "original"; //adds visual constraint to only grab content from elements with "original" in them as in "see original post for full detail
var URLforExtraction = "http://www.usajobs.gov/GetJob/ViewDetails/318047400"; //got from content grab, this original full post url
var APIKey = "57cc1e180d391c0b8cce51b7cdc175bd516ea4b8";

var indeedPublisherUsername = "will.lawrence@dzeegy.com"; //currently not used. Reference for logging into Indeed's publisher interface (https://ads.indeed.com/jobroll/xmlfeed)
var indeedPublisherPassword = "sharejPoint!"; //currently not used. Only as reference like above.

var contentURL = "";
var extraParams = "";

var http = require('http'),
q = require('querystring')

///* For ContentGrab
alchemyFullPath = alchemyPath + ContentGrab;
contentURL = URLofContent; 
extraParams = "&cquery=" + cquery; //adds visual constraint to only grab content from elements with cquery value.
//*/

/* For EntityExtraction
alchemyFullPath = alchemyPath + EntityExtraction;
contentURL = URLforExtraction; 
*/

/* For KeywordExtraction
alchemyFullPath = alchemyPath + KeywordExtraction;
contentURL = URLforExtraction; 
*/

http.createServer(function (req, res) {
	
    //grab results from passing a url to the entity enrichment Alchemy API
    /*var url=[alchemyFullPath
        ,"?"
		,"apikey="
        ,APIKey
        ,"&url="
        ,ContentURL
        ,extraParams
        ,].join(""),
		//create the options to pass into the get request
		options={hostname:alchemyHost
			,path:url};
    */
    
    //grab results from Indeed's XML web service
    var options = {hostname: IndeedHost 
        ,path: PathURLIndeedJobsWashingtonDC};     
    
	//a little lightweight logging to watch requests
	console.log("url:",options.hostname+options.path);
	console.log("requrl:",req.url);

	//make the request server side
	http.get(options,function(response){
		//console.log("hi",response);
		res.writeHead(200, {'Content-Type': 'text/xml'});
		var pageData = "";
	    response.setEncoding('utf8');
	    //stream the data into the response
	    response.on('data', function (chunk) {
	      pageData += chunk;
	    });

	    //write the data at the end
	    response.on('end', function(){
	      res.write(pageData);
	      res.end();
	    });

	});

}).listen(process.env.PORT || 3000);